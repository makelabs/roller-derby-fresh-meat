
# 5. Officiating

## 5.1. Staffing

**Each game must staff enough on-skates Officials (Referees) to effectively track the following information in real time:**

- the location of the Pack and Engagement Zone
- which Blockers are out of play
- who the Jammers are
- which Jammer (if any) is Lead Jammer
- how many points each Jammer has scored

One Referee is designated the Head Referee.

### 5.1.1. Distinction

**Referees are responsible for assessing and enforcing penalties, must be on skates, and must be uniformed in a manner that clearly identifies them as Referees.** Referees must be distinguishable from each other; for example, by displaying a name or number on their uniform.

### 5.1.2. Requirements

**Each game must also staff enough Officials to effectively track the state of the game such that the rules can be enforced in real time.** The number and position of Officials may vary based on available technology and the limitations of the venue, but the following information must be available upon request:

- the Official Score
- the Official Jam Time
- the Official Period Time
- which Skaters have been assessed how many penalties, and which have been served
- which Skaters are not allowed to skate (for example, due to expulsion, fouling out, or having a Jam called for their injury)
- how long a given Skater has been seated for each penalty

Individual Officials may be assigned to multiple tasks so long as this does not threaten the accuracy of the above information.

## 5.2. Duties

**All Officials are responsible for keeping the game running safely and smoothly by ensuring that the rules of the game are followed.** This includes but is not limited to:

- ensuring that each team has an acceptable number of Skaters on the track
- ensuring that each team has an acceptable number of Skaters in certain positions on the track
- ensuring that the game is played legally
- timing Jams, periods, penalties, and the time between Jams (including timeouts and reviews)
- signalling the starts and ends of Jams
- signalling who is the Lead Jammer
- signalling how many points each Jammer earns on each trip through the Pack
- informing Skaters and Team Staff of the state of the game when asked (to the best of their ability given the constraints of their responsibilities)
- calling Official Timeouts when additional time is needed. This may include a need to ensure that:
  - game information has been correctly recorded
  - gameplay is safe
  - injured Skaters have been taken care of
  - the teams are informed regarding anything out of order

Officials may call off Jams at their discretion. Reasons may include but are not limited to injury, technical difficulty, interference in a Jam by spectators or other Skaters, unsafe play, or illegal play that cannot be rectified via penalty assessment. If a Jam is called off due to officiating discretion when less than 30 seconds remain on the period clock at the end of the game, an additional Jam may be played at the discretion of the Officials. This additional Jam is the same type as the prior Jam (for example, an Overtime Jam follows a previous Overtime Jam).

## 5.3. Communication Between Skaters & Officials

**All communication between Skaters, Team Staff, and Officials must be respectful.**

**Officials should provide any information necessary for a Skater to know whether they are in play, including the location of the Pack.** Skaters who reasonably believe that they are in play should not be penalized for technical infractions that pertain to being out of play, unless such a warning has been given (examples include failure to return to play, to reform a Pack, or to yield after committing a false start).

**If an Official provides erroneous information to a Skater, the Skater will not be penalized for actions taken based on that information.** For example, if a Penalty Box Official releases a Skater early, the Skater will not be penalized for leaving once released. Likewise, if a Jammer calls off a Jam while their Jammer Referee is indicating that they are the Lead Jammer, said Jammer will not be penalized for calling off the Jam illegally, even if they are not in fact Lead. An absence of information provided (for example, an Official not providing a warning) is not considered as erroneous.

The Head Referee may, at their discretion, limit the extent to which Skaters may communicate with Officials.

## 5.4. Assessing Penalties

**All Referees may assess penalties to Skaters for illegal actions that have impact on the game.** Non-Skating Officials may assess penalties that are relevant to their position in the game, unless prohibited from doing so by the Head Referee. Officials will use their judgment under the guidelines set forth in the Rules of Flat Track Roller Derby Casebook. They must do so as soon as possible and at a volume sufficient to be heard by the penalized Skater and relevant Officials given the constraints of the venue. Until this has occurred, nobody is required to behave as if the Skater has been penalized.

**No penalty should be assessed unless the Official is certain that the penalty is warranted.** If Officials cannot agree on whether an action warrants a penalty, the Head Referee’s decision is final.

If the only Blocker from a team who is on the track commits a penalty, the Blocker should not be sent off the track until another Blocker from their team rejoins the Pack.

If a penalty is warranted, but it is not clear to whom the penalty should be assessed, an Official should assess the penalty to the nearest Blocker from the appropriate team if the action is committed mid-Jam, or to the team’s Captain if the action is committed between Jams. If an Official is not certain which team is responsible, no penalty should be assessed. If off-skates support staff commit a penalty, the penalty should be assessed to the appropriate team’s Captain. If a penalty is assessed to the Captain due to the fact that they are Captain, they will serve the penalty as a Blocker in a following Jam.
